from django.urls import path
from projects.views import projects_list, tasks_list, create_project


urlpatterns = [
    path("", projects_list, name="list_projects"),
    path("<int:id>/", tasks_list, name="show_project"),
    path("create/", create_project, name="create_project"),
]
